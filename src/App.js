import "./App.scss";
import BasicMap from "./components/Map/Basic";

function App() {
  return (
    <>
      <div className="App">
        <BasicMap />
      </div>
    </>
  );
}

export default App;
