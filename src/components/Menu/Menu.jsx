import React, { useState, useEffect } from 'react';
import './Menu.scss';
import { addDoc, collection } from 'firebase/firestore';
import { ref, uploadBytes, getDownloadURL } from 'firebase/storage';
import { db, storage } from '../Firebase/init-firebase';
import { RxCross1 } from 'react-icons/rx';  
import Modal from '../Modal/Modal'

const key_api = 'fe55b82c03fb4bd2814c96f301687a91';

const Menu = () => {
  const [visible, show] = useState(false);
  const [discription, setDiscription] = useState('');
  const [address, setAddress] = useState('');
  const [typeProblem, setTypeProblem] = useState('');
  const [imageUpload, setImageUpload] = useState('');
  const [coords, setCoords] = useState({});   
  const [isTrue] = useState(false); 
  const [isModalOpen, setModalOpen] = useState(false);

  const handleModalOpen = () => {
    setTimeout(() => {
      setModalOpen(true);
    }, 400);
  };
  
  const handleModalClose = () => {
    setModalOpen(false);
  };
  
  const options = [
    { value: 'Тип заявки' },
    { value: 'Открытый люк' },
    { value: 'Разбросанный мусор' },
    { value: 'Сухое дерево (обваленное дерево)' },
    { value: 'Пробитый трубопровод' },
    { value: 'Фонарь уличного освещения' },
    { value: 'Светофор не работает'},
    { value: 'Другая проблема'},
  ];
  
  function handleSelect(event) {
    setTypeProblem(event.target.value);
  }
  
  // Upload files (images)
  const uploadFile = (callback) => {
    if (!imageUpload) return;
    
    const imageRef = ref(storage, `images/${imageUpload.name}`);
    
    uploadBytes(imageRef, imageUpload).then((snapshot) => {
      getDownloadURL(snapshot.ref).then((url) => {
        callback(url);
      });
    });
  };
  
  useEffect(() => {
    const { latitude, longitude } = coords.coords || {};
    
    if (latitude && longitude) {
      fetch(`https://nominatim.openstreetmap.org/reverse?format=json&lat=${latitude}&lon=${longitude}`)
      .then((res) => res.json())
        .then((res) => {
          const fetchedLatitude = res.lat;
          const fetchedLongitude = res.lon;
          console.log(res);
          const coordinatesCollRef = collection(db, 'coordinates');
          uploadFile((url) => {
            addDoc(coordinatesCollRef, {  
              typeProblem,
              address,
              coordinates: [fetchedLatitude, fetchedLongitude],
              discription,
              uploadFile: url, 
              isTrue: isTrue,
            });
          });
        })
        .catch((error) => console.log(error));
      }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ coords]);
  
  const problenAddress = () => {
    navigator.geolocation.getCurrentPosition(onSuccess);
  };
  
  function onSuccess(position) {
    setCoords(position);
    console.log('Detecting your location...');
    const { latitude, longitude } = position.coords;
    fetch(`https://api.opencagedata.com/geocode/v1/json?q=${latitude}+${longitude}&key=${key_api}`)
    .then((response) => response.json()) 
    .then((response) => { 
      let roadAddress = response.results[0].components; 
      let { road, house_number, town, address } = roadAddress;
      if (house_number) {
        setAddress(`${road}, ${house_number}, ${town}`);
      } else if (town) {
        setAddress(`${road}, ${town}`);
      } else if (road) {
        setAddress(`${road}`);
      } else if(address) {
        setAddress();
      }
    })
    .catch(() => {
      console.log("Something went wrong");
    });
}  

  useEffect(() => {
    const timer = setTimeout(() => {
      problenAddress();
    }, 5000);
    
    return () => {
      clearTimeout(timer);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
 
  return (
    <>
      <div className="mobile">
        <div className="mobile__container">
          <button className="mobile__btn" onClick={() => show(!visible)}>
            Создать заявку
          </button>
        </div>
      </div>
      <button className="adding" onClick={() => show(!visible)}>
        <p className="adding_item">+</p>
      </button>
      <nav className={!visible ? 'navbar' : ''}>
        <div className="content-sidebar"> 
            <RxCross1 className="close" onClick={() => show(!visible)} />
            <h3 className="header">Помогите сделать город лучше!</h3>
            <select className="form-select" onChange={handleSelect}>
              {options.map((option, index) => (
                <option key={index} value={option.value}>
                  {option.value}
                </option>
              ))}
            </select>
            <label className="label">
              <span className="plus-input">+</span>
              <span className="text-input">Загрузить фото проблемы</span>
              <input
                className="input_label"
                type="file"
                onChange={(event) => {
                  setImageUpload(event.target.files[0]);
                }}
              />
            </label>
            <textarea
              id="name"
              value={discription}
              onChange={(e) => setDiscription(e.target.value)}
              className="comment"
              placeholder="Краткое описание" 
            ></textarea>
            <label>
              <input
                value={address}
                onChange={(e) => setAddress(e.target.value)}
                onClick={problenAddress}
                className="user-map"
                placeholder="Адрес, где проблема обнаружена" 
              />
              <i className='input-address-valid'>Адресная строка обязательна к нажатию.</i>
              <Modal open={isModalOpen} onClose={handleModalClose} />
              <button onClick={() => {
                handleModalOpen();
                uploadFile(() => {});
              }} className="user-map-done">
                  <p>Создать заявку</p>
            </button>
            </label> 
        </div> 
      </nav>
    </>
  );
};

export default Menu;
