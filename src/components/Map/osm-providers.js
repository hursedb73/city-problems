/* eslint-disable import/no-anonymous-default-export */
export default {
  maptiler: {
    url: "https://api.maptiler.com/maps/openstreetmap/256/{z}/{x}/{y}.jpg?key=60K7IaYoYPX8Qfdu1CUL",
    attribution: '&copy; <a href="https://itkb.kg/">IT Academy</a>',
  },
};
