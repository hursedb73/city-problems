import "./Basic.scss";
import React, { useState, useEffect } from "react";
import Menu from "../Menu/Menu"; // Импортируем компонент Menu
import { MapContainer, TileLayer, Marker, Popup } from "react-leaflet";
import osm from "./osm-providers";
import { useRef } from "react";
import "leaflet/dist/leaflet.css";
import useGeolocation from "./useGeolocation";
import MapClickHandler from "./MapClickHandler";
import L from "leaflet";
import { collection, getDocs } from "firebase/firestore";
import { db } from "../Firebase/init-firebase";
import { updateDoc, doc } from "firebase/firestore";
import { onSnapshot } from "firebase/firestore";

const markerIcon = new L.Icon({
  iconUrl: require("../../images/markerUser.png"),
  iconSize: [50, 55],
  iconAnchor: [35, 50],
  popupAnchor: [0, -46],
});

const customIcon = new L.icon({
  iconUrl: require("../../images/markerProblem.png"),
  iconSize: [32, 32],
  iconAnchor: [16, 32],
});

const BasicMap = () => {
  const [markers, setMarkers] = useState([]);
  const [marker, setMarker] = useState([]);
  const [filteredMarker, setFilteredMarker] = useState([]);
  const [center] = useState({
    map: 15,
    lat: 42.8022,
    lng: 73.8525,
  });

  const ZOOM_LEVEL = 14.4;
  const mapRef = useRef();
  const location = useGeolocation();

  const [coordinates, setCoordinates] = useState([0, 0]);

  const handleMapClick = (e) => {
    const { lat, lng } = e.latlng;
    setCoordinates([lat, lng]);
  };

  useEffect(() => {
    if (!marker.length > 0) {
      getMarkersData();
    }
  }, [marker]);

  const getMarkersData = async () => {
    const ref = collection(db, "coordinates");
    const dataSnapshot = await getDocs(ref);
    const dataList = dataSnapshot.docs.map((doc) => doc.data());

    const filteredDataList = dataList.filter((item) => item.yourField === true);

    setFilteredMarker(filteredDataList);
  };

  useEffect(() => {
    const markersCollectionRef = collection(db, "coordinates");

    const unsubscribe = onSnapshot(markersCollectionRef, (snapshot) => {
      const data = snapshot.docs.map((doc) => ({ id: doc.id, ...doc.data() }));
      setMarkers(data);
    });

    return () => unsubscribe();
  }, []);

  const updateIsTrueValue = async (documentId, newValue) => {
    const docRef = doc(db, "coordinates", documentId);
    await updateDoc(docRef, { isTrue: newValue });
  };

  return (
    <>
      <div className="row">
        <div className="col">
          <MapContainer center={center} zoom={ZOOM_LEVEL} ref={mapRef}>
            <TileLayer
              url={osm.maptiler.url}
              attribution={osm.maptiler.attribution}
            />
            <MapClickHandler onMapClick={handleMapClick} />
            <Menu
              coordinates={coordinates}
              updateIsTrueValue={updateIsTrueValue}
            />
            {markers.map(
              (item, index) =>
                item.isTrue && (
                  <Marker
                    icon={customIcon}
                    key={index}
                    position={item.coordinates}
                  >
                    <Popup>
                      <ul>
                        <li>Проблема: {item.typeProblem}</li>
                        <li>Описание: {item.discription}</li>
                        <li>Адрес: {item.address}</li>
                        {item.uploadFile && (
                          <li>
                            Фото проблемы:{" "}
                            <img
                              src={item.uploadFile}
                              className="popup-image"
                              alt="Фото проблемы"
                            />
                          </li>
                        )}
                      </ul>
                    </Popup>
                  </Marker>
                )
            )}
            {location.loaded && !location.error && (
              <Marker
                icon={markerIcon}
                position={[location.coordinates.lat, location.coordinates.lng]}
              />
            )}
          </MapContainer>
        </div>
      </div>
    </>
  );
};

export default BasicMap;
