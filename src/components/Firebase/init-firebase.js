import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";

import { getFirestore } from "@firebase/firestore";
import { getStorage } from "firebase/storage";
import "firebase/storage";

export const firebaseConfig = {
  apiKey: "AIzaSyBr8gb6ZC2THMxx-D9pmcWGdSuxZ2dvfp4",
  authDomain: "my-city-386711.firebaseapp.com",
  projectId: "my-city-386711",
  storageBucket: "my-city-386711.appspot.com",
  messagingSenderId: "545617996306",
  appId: "1:545617996306:web:4dacb2f5a9e9dd54b4c6cb",
  measurementId: "G-GTSB0QPB8S",
};

const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);

export const db = getFirestore(app);
export const storage = getStorage(app);
