 import './Modal.scss'
import { RxCross1 } from 'react-icons/rx';

const Modal = ({ open, onClose }) => {
 
        const handlePageReload = () => {
          window.location.reload();
        };

  if (!open) return null;
  return (
    <div onClick={onClose} className='overlay'>
      <div
        onClick={(e) => {
          e.stopPropagation();
        }}
        className='modalContainer'
      > 
        <div className='modalRight'>
        <RxCross1 className="close-modal" onClick={onClose} />
          <div className='content'> 
            <h1>Успешно добавлено!</h1> 
            <p>Ваша информация поможет сделать наш город лучше.</p>
          </div>
          <div className='btnContainer'>
            <button  className='btnPrimary'>
              <span className='bold'onClick={onClose} >Добавить новую проблему</span>
            </button>
            <button className='btnOutline'  onClick={handlePageReload}>
              <span className='bold'>Закрыть</span>
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Modal;
